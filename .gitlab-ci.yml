image: alpine:latest

variables:
  POSTGRES_USER: user
  POSTGRES_PASSWORD: testing-password
  POSTGRES_ENABLED: "true"
  POSTGRES_DB: $CI_ENVIRONMENT_SLUG
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501

stages:
  - build
  - test
  - deploy  # dummy stage to follow the template guidelines
  - review
  - dast
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - cleanup

workflow:
  rules:
    - if: '$BUILDPACK_URL || $AUTO_DEVOPS_EXPLICITLY_ENABLED == "1" || $DOCKERFILE_PATH'

    - exists:
        - Dockerfile

    # https://github.com/heroku/heroku-buildpack-clojure
    - exists:
        - project.clj

    # https://github.com/heroku/heroku-buildpack-go
    - exists:
        - go.mod
        - Gopkg.mod
        - Godeps/Godeps.json
        - vendor/vendor.json
        - glide.yaml
        - src/**/*.go

    # https://github.com/heroku/heroku-buildpack-gradle
    - exists:
        - gradlew
        - build.gradle
        - settings.gradle

    # https://github.com/heroku/heroku-buildpack-java
    - exists:
        - pom.xml
        - pom.atom
        - pom.clj
        - pom.groovy
        - pom.rb
        - pom.scala
        - pom.yaml
        - pom.yml

    # https://github.com/heroku/heroku-buildpack-multi
    - exists:
        - .buildpacks

    # https://github.com/heroku/heroku-buildpack-nodejs
    - exists:
        - package.json

    # https://github.com/heroku/heroku-buildpack-php
    - exists:
        - composer.json
        - index.php

    # https://github.com/heroku/heroku-buildpack-play
    # TODO: detect script excludes some scala files
    - exists:
        - '**/conf/application.conf'

    # https://github.com/heroku/heroku-buildpack-python
    # TODO: detect script checks that all of these exist, not any
    - exists:
        - requirements.txt
        - setup.py
        - Pipfile

    # https://github.com/heroku/heroku-buildpack-ruby
    - exists:
        - Gemfile

    # https://github.com/heroku/heroku-buildpack-scala
    - exists:
        - '*.sbt'
        - project/*.scala
        - .sbt/*.scala
        - project/build.properties

    # https://github.com/dokku/buildpack-nginx
    - exists:
        - .static

# Override the test job
test:
  # Run this in a python:3.7 container (should have all the dependencies we need)
  image: python:3.7
  # Install poetry and install project dependencies with it
  before_script:
    - pip install poetry
    - poetry install
  # Run the unit tests, outputting them to an XML file
  script:
    - poetry run pytest --junitxml=report.xml tests/
  # Upload that XML file to GitLab to display the results in a nice way
  artifacts:
    when: always
    reports:
      junit: report.xml

include:
  - template: Jobs/Build.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml
  - template: Jobs/Test.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Test.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Code-Intelligence.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Intelligence.gitlab-ci.yml
  - template: Jobs/Deploy.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml
  - template: Jobs/Deploy/ECS.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy/ECS.gitlab-ci.yml
  - template: Jobs/Deploy/EC2.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy/EC2.gitlab-ci.yml
  - template: Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml
  - template: Jobs/Browser-Performance-Testing.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml
  - template: Security/DAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
